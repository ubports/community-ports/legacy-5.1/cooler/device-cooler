# Device image for cooler

This repository contains the *prebuilt*, *binary* objects which are packed into the device tree for the BQ M10 HD, codename `cooler`. Not to be confused with the BQ M10 FHD, codename `frieza`.
